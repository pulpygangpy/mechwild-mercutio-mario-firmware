# MechWild Mercutio Mario Firmware

## Description
Custom firmware for MechWild Mercutio Keyboard with Mario Bros. 3 animation based on the awesome [bongo-cat firmware](https://github.com/qmk/qmk_firmware/tree/master/keyboards/mechwild/mercutio/keymaps/bongocat) found in the [official QMK firmwares repository for the MechWild Mercutio](https://github.com/qmk/qmk_firmware/tree/master/keyboards/mechwild/mercutio).

## Animation
![Animation < 40WPM](assets/gifs/mario-run.gif)

Animation < 40 WPM

![Animation > 40WPM](assets/gifs/mario-run-full.gif)

Animation > 40 WPM

## How?
I followed the steps for image creation found in this article: [How do I convert an image for use on an OLED display?](https://docs.splitkb.com/hc/en-us/articles/360013811280-How-do-I-convert-an-image-for-use-on-an-OLED-display-)

Basically:
- You need to create an image in the resolution of the display (128x32 for Mercutio)
- I used [Pixilart](https://www.pixilart.com/) for creating the sprites with the white pencil and black background
- Convert the frames to code using [image2cpp](https://javl.github.io/image2cpp/) online tool
- Make sure to select "plain bytes" in *code output* and "vertical -1 bit per pixel" in *draw mode*
- Replace the code of the images in the array

[Here](assets/mario.pixil) you can find my pixil save file.
## Installation
You can compile the source code for the keymap or found my latest build for the firmware HEX in releases.

## DISCLAIMER
I'm not responsible for any damage that may occour to your board during flashing. This is a keymap that was meant for the MechWild Mercutio which has the ATMega328 controller, and while it is working for me, I cannto guarantee it will work for you.

## TODO
- [x] Mario animation
- [ ] Display layer and caps lock
- [ ] Useful information (clock, volumen, etc.)
- [ ] ~~Actually learn to use a 40%~~ ?

## Authors and acknowledgment
This was based on the bongo-cat firmware, so kudos to the authors and the awesome community that is MechWild and the QMK project.